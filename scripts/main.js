const NR_OF_ROWS_PRICE_LIST = 10;
let products = [];

function createCellEl(content, tag) {
  let elCell = document.createElement(tag);
  elCell.innerHTML = content;
  return elCell;
}

function createRowEl(entries, isHead) {
  let elRow = document.createElement('tr');
  for (let entry of entries) {
    elRow.appendChild(createCellEl(entry, isHead ? 'th' : 'td'));
  }
  return elRow;
}

function createPriceList(products) {
  const elTHead = document.querySelector('table thead');
  const elListSecClasses = document.getElementById('list-section').classList;
  // remove all entries in table header
  elTHead.innerHTML = '';
  if (products.length > 0) {
    // make sure that section is displayed
    elListSecClasses.remove('do-not-display');
    // create entries in table header
    const trHeadEntries = ['Anz.'];
    for (const product of products) {
      trHeadEntries.push(product.name);
    }
    const newHeadRow = createRowEl(trHeadEntries, true);
    elTHead.appendChild(newHeadRow);

    const elTBody = document.querySelector('#price-list tbody');
    // remove all entries in table body
    elTBody.innerHTML = '';
    // create entries in table body
    for (let i = 0; i < NR_OF_ROWS_PRICE_LIST; i++) {
      const trBodyEntries = [i + 1];
      for (const product of products) {
        trBodyEntries.push(product.price * (i + 1));
      }
      const newBodyRow = createRowEl(trBodyEntries, false);
      elTBody.appendChild(newBodyRow);
    }
  } else {
    // do not display section
    elListSecClasses.add('do-not-display');
  }
}

function updateSectionToDelete(products) {
  const elProdList = document.querySelector('#del-list tbody');
  const elDelSecClasses = document.getElementById('del-section').classList;
  // remove all list items
  elProdList.innerHTML = '';
  if (products.length > 0) {
    // make sure that section is displayed
    elDelSecClasses.remove('do-not-display');
    // add list items
    for (let i = 0; i < products.length; i++) {
      const productName = products[i].name;
      const delButton = `<button onclick=removeProduct(${i})>Löschen</botton>`;
      const elRow = createRowEl([productName, delButton], false);
      elProdList.appendChild(elRow);
    }
  } else {
    // do not display section
    elDelSecClasses.add('do-not-display');
  }
}

function updateData(products) {
  updateSectionToDelete(products);
  createPriceList(products);
}

function removeProduct(prodIndex) {
  products.splice(prodIndex, 1);
  updateData(products);
}

function addProduct() {
  const elProdInput = document.getElementById('product');
  const elPriceInput = document.getElementById('price');
  const prodInput = elProdInput.value.trim();
  const priceInput = parseInt(elPriceInput.value);
  if (prodInput && priceInput) {
    products.push({ name: prodInput, price: priceInput });
    updateData(products);
    // remove entered inputs
    elProdInput.value = '';
    elPriceInput.value = '';
  }
}
